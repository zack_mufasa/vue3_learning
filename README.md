
# 目录

[TOC]

## 开始

### 基础

#### 创建实例

1. 通过Vue.createApp创建**根组件**：`createApp(App).use(store).use(router).mount('#app')`
2. 如下：mount函数返回的不是应用本身，而是根组件实例（vm通常用来表示组件实例）

```javascript
const app = Vue.createApp({
  data() {
    return { count: 4 }
  }})
const vm = app.mount('#app')
console.log(vm.count) // => 4
```

3. 生命周期与vue2的不同之处：**beforeUnmount、unmounted**替换原来的**beforeDestroy以及destroyed**
4. 不要在选项或者回调中使用箭头函数，会报undefined错误：Uncaught TypeError: Cannot read property of undefined

#### 模板语法

1. 一次性插值：`v-once`，第一次绑定之后就丧失响应性： `<span v-once>{{msg}}</span>`
2. `v-html`可能会非常危险，容易导致XSS攻击，**只对可信内容使用 HTML 插值，绝不要将用户提供的内容作为插值**
3. `v-bind`
4. `v-on`
4. `v-for`
5. `v-if/v-else`
6. 修饰符（`.prevent`调用`event.preventDefault()`）

#### data属性以及方法

1. 组件data是一个函数，保存在组件实例的`$data`中：`vm.$data`
2. methods
3. 防抖节流：官网推荐使用lodash等库实现

#### 计算属性computed和侦听器watch

1. 组件中的computed属性与vue2保持一致，都可以设置getter、setter
2. 组件内的watch与vue2保持一致
3. computed与watch对比
    * 相同点：
        1. watch和computed都是以Vue的依赖追踪机制为基础的，它们都试图处理这样一件事情：当某一个数据（称它为依赖数据）发生变化的时候，所有依赖这个数据的“相关”数据“自动”发生变化，也就是自动调用相关的函数去实现数据的变动。
    * 不同点：
        1. computed支持缓存，只有依赖数据发生改变,才会重新进行计算;而watch不支持缓存，数据变，直接会触发相应的操作。
        2. computed不支持异步（不能发请求） ，当computed内有异步操作时无效，无法监听数据的变化;而watch支持异步
        3. computed属性值会默认走缓存，是基于它们的响应式依赖进行缓存的，而watch监听的函数接收两个参数，第一个参数是最新的值，第二个参数是输入之前的值
        4. computed中的属性都有一个get和一 个set方法，当数据变化时，调用set方法
        5. computed必须return，watch不需要
        6. computed时多对一或者一对一的，一对多的时候一般用watch（多个依赖计算得到一个属性使用computed，一个属性变化操作其它的属性/方法使用watch）

#### Class与Style的绑定

1. `v-bind:class`语法与vue2保持一致
2. 组件my_comp只有单个根元素（假定为一个带efg的class的div），则父组件中的`<my_comp class="abc"></my_comp>` 的class会被应用到该根元素上：`<div class="efg abc"></div>`
3. 如果有多个根元素则需要从$attrs.class获取
4. `v-vind:style`语法与vue2保持一致

```html
<div v-bind:style="{ color: activeColor, fontSize: fontSize + 'px' }"></div>
```

#### 条件渲染

1. `v-if/v-else/v-else-if` ： 使用带`v-if`的`template`元素包裹，最终`template`不会被渲染
2. `v-show`
3. `v-if`与`v-show`对比：
    * v-if是控制渲染，组件被销毁或者重建，v-show是控制css的display样式为默认值/none
    * v-if初始条件为假的时候不进行渲染，直到为真的时候才渲染；v-show不论初始条件真假总会被渲染，仅仅进行css切换。一般来说，v-if 有更高的切换开销，而 v-show 有更高的初始渲染开销。因此，如果需要非常频繁地切换，则使用 v-show 较好；如果在运行时条件很少改变，则使用 v-if 较好。
    * **vue2/vue3中的区别**：同级使用，在vue2中v-for的优先级更高，vue3中v-if的优先级更高，并且vue3中会报错。（vue2源码使用if/else判断对v-for的判断条件在v-if前，vue3中使用switch判断并且对v-if的判断case在v-for前）

#### 列表渲染

1. `v-for="item in items"`也可以使用`v-for="item of items"`更符合js的迭代器用法
2. 也可以遍历对象的属性（以`Object.keys()`结果进行遍历）：`v-for="(value, name, index) in myObject"`
3. 你需要为每项提供一个唯一的 key（字符串/数组类型），作为 Vue 的虚拟 DOM 算法的提示，以保持对节点身份的持续跟踪。这样 Vue 就可以知道何时能够重用和修补现有节点，以及何时需要对它们重新排序或重新创建

#### 事件处理

1. `v-on:click`/`@click`，接收原生的Dom Event：调用方法时传入`$event`
2. 多事件处理器：`@click="one($event), two($event)"`，可以同时触发两个事件，不过必须加上括号()否则会把它看成普通的js变量语句（同vue2）
3. 按键修饰符 ······

#### 表单输入绑定

1. text 和 textarea 元素使用 value property 和 input 事件；
2. checkbox 和 radio 使用 checked property 和 change 事件；
3. select 字段将 value 作为 prop 并将 change 作为事件。

#### 组件基础+深入

1. 全局注册：`app.component` ；局部注册：通过components选项来引入自定义的组件，与vue2保持一致
2. prop是单向数据流，单向下行绑定：父级 prop 的更新会向下流动到子组件中，但是反过来则不行。如果在子组件内部改变prop，控制台vue会发出警告。如果子组件有需要改动，可以把prop赋值到data/computed中，但是引用类型仍然会改动父组件状态，因此应该避免修改任何 prop。
3. 监听子组件事件/自定义事件：同vue2一样使用$emit，**但是vue3新增了emits选项：用来定义一个组件可以向其父组件触发的事件**
4. 组件中使用v-model
    1. 父组件
        * prop：`:value` -> `:modelValue='my_value'`；
        * 事件：`@input` -> `@update:modelValue='my_value=$event'`
    2. 子组件中要定义默认的props:`modelValue`，并且需更改父组件值时调用`$emit('update:modelValue',$event.target.value)`
5. 动态组件：使用`is=xx`来定义一个动态组件,使用`keep-alive`标签来缓存组件的状态

```html
<keep-alive>
  <component :is="currentTabComponent"></component>
</keep-alive>
```

6. 异步组件：使用`defineAsyncComponent`包裹一个返回值为`Promise`的函数，如：

```javascript
const AsyncComp = defineAsyncComponent(() =>
  import('./components/AsyncComponent.vue')
)
```

## API参考

### 选项

选项式API，与vue2的选项式API概念类似，可以作为实例的“属性”

#### 组合

##### mixins

1. 类型：Array<Object>
2. mixins类似于正常实例，可以包含多个选项，包含的生命周期会在使用该mixins的组件之前调用
2. mixins存在相同的data会被组件内的data覆盖
3. Vue3 继续支持 mixin 的同时，组合式 API是更推荐的在组件之间共享代码的方式，即：使用setup选项代替mixins

##### extends

1. 类型：Object
2. 扩展，从实现的角度看，extends 几乎等同于 mixins，然而，extends 和 mixins 表达了不同的意图。mixins 选项主要用来组合功能，而 extends 主要用来考虑继承性

##### provide/inject

1. provide 选项应该是一个对象或返回一个对象的函数 ：Object | () => Object
2. inject应该是：Array<string> | { [key: string]: string | Symbol | Object }

##### setup

1. 类型：Function
2. 参数：setup(props, context)  
    1. props是响应式的，但是解构props会使其丢失响应式，如果需要解构props可以使用`const {...}=toRefs(props)`，或者`const v=toRef(props, 'key')`；
    2. context包含attrs、slots 、emit以及expose（expose是一个函数，允许暴露特定的属性），其中attrs、slots是响应式的，是内部组件实例上相应值的代理，可以直接解构；
    3. 组件使用 props 比其他 property 更常见，并且很多情况下组件仅使用 props。
    4. 将 props 作为单独的参数可以使单独键入更容易，而不会弄乱上下文中其他 property 的类型。这也使得在具有 TSX 支持的 setup、render 和普通功能组件之间保持一致的签名成为可能。
3. 调用时间：初始 prop 解析之后立即调用 setup，在生命周期方面，它是在 beforeCreate 钩子之前调用的。
4. setup 返回的 `ref` 在模板中访问时会自动解包，因此模板中不需要 .value，只要`<template>`内包裹的都不需要使用.value，但是在组件其他部分的js代码中是需要的

##### reactive

##### Refs

1. `ref('abc')`接受一个内部值并返回一个响应式且可变的 `ref` 对象，ref需要通过`.value`进行访问其值（`template`内除外）
2. `toRef(props, 'key')`用来为源响应式对象上的某个 key 新创建一个 ref
3. `toRefs(props)`将响应式对象转化成普通的对象，每一个property都是源property的ref

```javascript
let ref_obj = ref('ref内部值');
console.log(`ref_obj.value：${ref_obj.value}`);

let toRef_prop_user = toRef(props, 'user');
console.log(`toRef_prop_user.value: ${toRef_prop_user.value}`);//是一个ref，所以要用.value进行访问值

let toRefs_obj = toRefs(props);
console.log(`toRefs_obj.value: `);
console.log(toRefs_obj.user);
console.log(toRefs_obj.user.value);//具体属性是ref，所以还是需要加.value
```

## 对比Vue2.x

1. **新增**：
    1. vue2从技术上讲并没有‘app’这个概念，我们定义的应用只是通过 new Vue() 创建的根 Vue 实例；vue3新的全局API：Vue.createApp：`const app = createApp({})`，
    2. vue2中任何全局改变 Vue 行为的 API 在vue3中都会移动到应用实例API上，如`Vue.use`变成了`app.use`，`Vue.mixin`变成`app.mixin`、`Vue.component`变成`app.component`等等
    3. 和 prop 类似，vue3新增`emits`选项来定义组件可触发的事件
    4. `teleport` 提供了一种干净的方法，允许我们控制在 DOM 中哪个父节点下渲染了 HTML

    ```html
    <!--控制div在body的第一级子元素显示，以body为父元素-->
     <teleport to="body">
      <div v-if="modalOpen" class="modal">
      </div>
    </teleport>
    ```

    5. **新增`setup`组合式API**
2. **移除**
    1. `$children`被移除，推荐用$refs来访问子组件
    2. `$listeners`被移除，事件监听器变为`$attrs`的一部分
    3. `propsData`选项被移除
    4. 全局函数 set 和 delete 以及实例方法 $set 和 $delete。基于代理的变化检测已经不再需要它们了
3. **修改**
    1. 生命周期与vue2的不同之处：**beforeUnmount、unmounted**替换原来的**beforeDestroy以及destroyed**
    2. Vue 3 现在正式支持了**多根节点**的组件，也就是片段
    3. 在vue2中`v-for`的优先级更高，vue3中`v-if`的优先级更高，并且vue3中会报错。（vue2源码使用`if/else`判断对`v-for`的判断条件在`v-if`前，vue3中使用`switch`判断并且对`v-if`的判断`case`在`v-for`前）
    4. 自定义组件上，v-model prop 和事件默认名称已更改：
        * prop：`:value` -> `:modelValue`；
        * 事件：`@input` -> `@update:modelValue`
        * 组件现在现在可以绑定多个v-model
    5. `data`只能是返回对象的函数，不可以是对象
    6. 异步组件以前是通过返回 Promise 的函数来创建的`const asyncModal = () => import('./Modal.vue')` ；vue3中需要使用`defineAsyncComponent`方法包裹 `const asyncModal = defineAsyncComponent(() => import('./Modal.vue'))`或者`const asyncModalWithOptions = defineAsyncComponent({loader: () => import('./Modal.vue') })`  原先的component选项字段被替换成`loader`选项字段
    7. `$attrs` 现在包含了所有传递给组件的属性，包括 `class` 和`style`
    8. 自定义指令的生命周期和组件的生命周期保持一致，但是没有`beforeCreate()`；访问组件实例通过binding参数而不是vnode参数
4. **原理**
    1. proxy代理整个对象而非Object.defineProperty
    2. diff算法优化
