import { createRouter, createWebHashHistory } from 'vue-router'
import Home from '../views/Home.vue'
import More from '../views/More.vue';

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  }, {
    path: '/more',
    name: 'More',
    // component: ()=>import ('../views/More.vue'),//使用路由组件懒加载，等点击对应的路由才请求服务器得到对应的路由组件，减少首页渲染压力
    component: More
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
